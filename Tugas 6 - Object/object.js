// Soal 1
console.log('Soal 1')


function arrayToObject(data) {
    if (data.length > 0) {
        var objData = {}
        for (var i = 0; i < data.length; i++) {
            var now = new Date()
            var thisYear = now.getFullYear()
            var age = ''
            var year = data[i][3]
            if (!year || year > thisYear) {
                age = 'Invalid Birth Year'
            } else {
                age = thisYear - year
            }
            objData.firstName = data[i][0]
            objData.lastName = data[i][1]
            objData.gender = data[i][2]
            objData.age = age

            console.log(i+1 + '. ' + objData.firstName + ' ' + objData.lastName + ' : ')
            console.log(objData)
        }
    } else {
        console.log("")
    }
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)

arrayToObject([])

console.log()

// Soal 2
console.log('Soal 2')
function shoppingTime(memberId, money) {
    var barang = [
        {
            name: 'Sepatu Stacattu',
            harga: 1500000
        },
        {
            name: 'Baju Zoro',
            harga: 500000
        },
        {
            name: 'Baju H&N',
            harga: 250000
        },
        {
            name: 'Sweater Uniklooh',
            harga: 175000
        },
        {
            name: 'Casing Handphone',
            harga: 50000
        }
    ]

    var invoice = {
        memberId: memberId,
        money: money,
        listPurchased: [],
        changeMoney: 0
    }

    if (memberId === '' || !memberId || !money) {
        return 'Mohon maaf, toko X hanya berlaku untuk member saja'
    } else if(money < 50000) {
        return 'Mohon maaf, uang tidak cukup'
    } else {
        var listPurchased = []
        var changeMoney = null

        for (var i = 0; i < barang.length; i++) {
            if (money >= barang[i].harga) {
                listPurchased.push(barang[i].name)
                money -= barang[i].harga
            } else {
                changeMoney = money
            }
        }
        invoice.listPurchased = listPurchased
        invoice.changeMoney = money

        return invoice
    }
}

console.log(shoppingTime('1820RzKrnWn08', 2475000))
console.log(shoppingTime('82Ku8Ma742', 170000))
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); //Mohon maaf, toko X hanya berlaku untuk member saja

console.log()

// Soal 3
console.log('Soal 3')
function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F']

    var arrObjPenumpang = []

    if (arrPenumpang.length > 0) {
        for (var i = 0; i < arrPenumpang.length; i++) {
            var objPenumpang = {}
            var asal = arrPenumpang[i][1]
            var tujuan = arrPenumpang[i][2]

            var indexAsal
            var indexTujuan

            for (var j = 0; j < rute.length; j++) {
                if (rute[j] === asal) {
                    indexAsal = j
                } else if (rute[j] === tujuan) {
                    indexTujuan = j
                }
            }
            var bayar = (indexTujuan-indexAsal)*2000

            objPenumpang.penumpang = arrPenumpang[i][0]
            objPenumpang.asal = asal
            objPenumpang.tujuan = tujuan
            objPenumpang.bayar = bayar

            arrObjPenumpang.push(objPenumpang)
        }
        return arrObjPenumpang
    } else {
        return []
    }
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]