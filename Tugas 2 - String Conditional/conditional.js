// If-else
console.log('Soal If-else')
var nama = 'John'
var peran = ''

if (nama === '' && peran === '') {
    console.log('Nama harus diisi!')
} else if (nama === 'John' && peran === '') {
    console.log('Halo '+ nama + ', Pilih peranmu untuk memulai game!')
} else if (nama === 'Jane' && peran === 'Penyihir') {
    console.log('Selamat datang di Dunia Werewolf, ' + nama)
    console.log('Halo ' + peran + ' ' + nama + ', kamu dapat melihat siapa yang menjadi werewolf!')
} else if (nama === 'Jenita' && peran === 'Guard') {
    console.log('Selamat datang di Dunia Werewolf, ' + nama)
    console.log('Halo ' + peran + ' ' + nama + ', kamu akan membantu melindungi temanmu dari serangan werewolf.')
} else if (nama === 'Junaedi' && peran === 'Werewolf') {
    console.log('Selamat datang di Dunia Werewolf, ' + nama)
    console.log('Halo ' + peran + ' ' + nama + ', Kamu akan memakan mangsa setiap malam!')
}
console.log()


// If-else
console.log('Switch Case')

var tanggal = 21; 
var bulan = 1; 
var tahun = 1945;

var namaBulan = ''

switch (bulan) {
    case 1:
        namaBulan = 'Januari'
        break;
    case 2:
        namaBulan = 'Februari'
        break;
    case 3:
        namaBulan = 'Maret'
        break;
    case 4:
        namaBulan = 'April'
        break;
    case 5:
        namaBulan = 'Mei'
        break;
    case 6:
        namaBulan = 'Juni'
        break;
    case 7:
        namaBulan = 'Juli'
        break;
    case 8:
        namaBulan = 'Agustus'
        break;
    case 9:
        namaBulan = 'September'
        break;
    case 10:
        namaBulan = 'Oktober'
        break;
    case 11:
        namaBulan = 'November'
        break;
    case 12:
        namaBulan = 'Desember'
        break;

    default:
        break;
}
console.log(tanggal + ' ' + namaBulan + ' ' + tahun)