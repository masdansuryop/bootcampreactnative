// Soal 1
console.log('Soal 1')
function range(startNum = -1, finishNum = -1) {
    var arrNum = []
    if (startNum === -1 || finishNum === -1) {
        arrNum = -1
    } else if (startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i++) {
            arrNum.push(i)
        }
    } else {
        for (var i = startNum; i >= finishNum; i--) {
            arrNum.push(i)
        }
    }
    return arrNum
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1
console.log()

// Soal 2
console.log('Soal 2')
function rangeWithStep(startNum = 0, finishNum = 0, step) {
    var arrNum = []
    if (startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i+=step) {
            arrNum.push(i)
        }
    } else {
        for (var i = startNum; i >= finishNum; i-=step) {
            arrNum.push(i)
        }
    }
    return arrNum
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
console.log()

// Soal 3
console.log('Soal 3')
function sum(startNum = 0, finishNum = 0, step = 1) {
    var sum = 0
    if (startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i+=step) {
            sum+=i
        }
    } else {
        for (var i = startNum; i >= finishNum; i-=step) {
            sum+=i
        }
    }
    return sum
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0
console.log()

// Soal 4
console.log('Soal 4')
function dataHandling(arrData) {
    for (var i = 0; i < arrData.length; i++) {
        console.log('Nomor ID: ' + arrData[i][0])
        console.log('Nama Lengkap: ' + arrData[i][1])
        console.log('TTL: ' + arrData[i][2] + ', ' + arrData[i][3])
        console.log('Hobi: ' + arrData[i][4])
        console.log()
    }
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

dataHandling(input)

// Soal 5
console.log('Soal 5')
function balikKata(kata) {
    var reverseKata = ''
    for (var i = kata.length-1; i >= 0; i--) {
        reverseKata+=kata[i]
    }
    return reverseKata
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I
console.log()

// Soal 6
console.log('Soal 6')
function dataHandling2(arrData) {
    arrData.splice(1, 1, 'Roman Alamsyah Elsharawy')
    arrData.splice(2, 1, 'Provinsi Bandar Lampung')
    arrData.splice(4, 2, 'Pria')
    arrData.splice(5, 0, 'SMA Internasional Metro')
    console.log(arrData)

    var splitDate1 = arrData[3].split('/')
    switch (splitDate1[1]) {
        case '01':
            console.log('Januari')
            break;
        case '02':
            console.log('Februari')
            break;
        case '03':
            console.log('Maret')
            break;
        case '04':
            console.log('April')
            break;
        case '05':
            console.log('Mei')
            break;
        case '06':
            console.log('Juni')
            break;
        case '07':
            console.log('Juli')
            break;
        case '08':
            console.log('Agustus')
            break;
        case '09':
            console.log('September')
            break;
        case '10':
            console.log('Oktober')
            break;
        case '11':
            console.log('November')
            break;
        case '12':
            console.log('Desember')
            break;
    
        default:
            break;
    }

    console.log(splitDate1.sort(function (value1, value2) { return value2 - value1 } ))

    var splitDate2 = arrData[3].split('/')
    console.log(splitDate2.join('-'))

    console.log(arrData[1].slice(0, 14))
}

var input2 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]

dataHandling2(input2)