var readBooksPromise = require('./promise.js')

var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

const time = 10000
const i = 0

function membaca(time, i) {
    readBooksPromise(time, books[i])
    .then((sisaWaktu) => {
        if (i < books.length-1) {
            if (sisaWaktu >= books[i].timeSpent) {
                membaca(sisaWaktu, i+=1)
            }
        } else {
            membaca(sisaWaktu, 0)
        }
    }).catch((err) => {
        return err
    });
}

membaca(time, i)