var readBooks = require('./callback.js')

var books = [
    {name: 'LOTR', timeSpent: 3000},
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000}
]

function membaca(time, i) {
    readBooks(time, books[i], function (sisaWaktu) {
        if (i < books.length-1) {
            if (sisaWaktu >= books[i].timeSpent) {
                membaca(sisaWaktu, i+=1)
            }
        } else {
            membaca(sisaWaktu, 0)
        }
    })
}

const time = 10000
const index = 0

membaca(time, index)