import React from "react";
import { Text, StyleSheet } from 'react-native'

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";

import { AuthContext } from "./Context";
import LoginScreen from "./LoginScreen";
import RegisterScreen from "./RegisterScreen";
import SkillScreen from "./SkillScreen";
import ProjectScreen from "./ProjectScreen";
import AddScreen from "./AddScreen";
import AboutScreen from "./AboutScreen";

const AuthStack = createStackNavigator();
const AuthStackScreen = () => (
    <AuthStack.Navigator headerMode="none">
        <AuthStack.Screen
            name="Login"
            component={LoginScreen}
        />
        <AuthStack.Screen
            name="Register"
            component={RegisterScreen}
        />
    </AuthStack.Navigator>
);

const Tabs = createBottomTabNavigator();

const SkillStack = createStackNavigator();
const SkillStackScreen = () => (
    <SkillStack.Navigator>
        <SkillStack.Screen name="Skill" component={SkillScreen} />
    </SkillStack.Navigator>
);

const ProjectStack = createStackNavigator();
const ProjectStackScreen = () => (
    <ProjectStack.Navigator>
        <ProjectStack.Screen name="Project" component={ProjectScreen} />
    </ProjectStack.Navigator>
);

const AddStack = createStackNavigator();
const AddStackScreen = () => (
    <AddStack.Navigator>
        <AddStack.Screen name="Add" component={AddScreen} />
    </AddStack.Navigator>
);

const AboutStack = createStackNavigator();
const AboutStackScreen = () => (
    <AboutStack.Navigator headerMode="none">
        <AboutStack.Screen name="About" component={AboutScreen} />
    </AboutStack.Navigator>
);

const TabsScreen = () => (
    <Tabs.Navigator>
        <Tabs.Screen name="Skill" component={SkillStackScreen} />
        <Tabs.Screen name="Project" component={ProjectStackScreen} />
        <Tabs.Screen name="Add" component={AddStackScreen} />
    </Tabs.Navigator>
);

const Drawer = createDrawerNavigator();
const DrawerScreen = () => (
    <Drawer.Navigator initialRouteName="Profile">
        <Drawer.Screen name="Skill" component={TabsScreen} />
        <Drawer.Screen name="About" component={AboutStackScreen} />
    </Drawer.Navigator>
);

const RootStack = createStackNavigator();
const RootStackScreen = ({ userToken }) => (
    <RootStack.Navigator headerMode="none">
        {userToken ? (
            <RootStack.Screen
                name="App"
                component={DrawerScreen}
                options={{
                    animationEnabled: false
                }}
            />
        ) : (
            <RootStack.Screen
                name="Auth"
                component={AuthStackScreen}
                options={{
                animationEnabled: false
                }}
            />
        )}
    </RootStack.Navigator>
);

const Splash = () => (
    <Text>Loading...</Text>
)

export default () => {
    const [isLoading, setIsLoading] = React.useState(true);
    const [userToken, setUserToken] = React.useState(null);

    const authContext = React.useMemo(() => {
        return {
            login: () => {
                setIsLoading(false);
                setUserToken("asdf");
            },
            register: () => {
                setIsLoading(false);
                setUserToken("asdf");
            },
            logout: () => {
                setIsLoading(false);
                setUserToken(null);
            }
        };
    }, []);

    React.useEffect(() => {
        setTimeout(() => {
            setIsLoading(false);
        }, 1000);
    }, []);

    if (isLoading) {
        return <Splash />;
    }

    return (
        <AuthContext.Provider value={authContext}>
            <NavigationContainer style={styles.container}>
                <RootStackScreen userToken={userToken} />
            </NavigationContainer>
        </AuthContext.Provider>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    }
});