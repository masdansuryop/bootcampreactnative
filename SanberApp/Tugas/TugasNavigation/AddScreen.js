import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

export default function AddScreen (){
    return (
        <View style={styles.container}>
            <Text>Halaman AddScreen</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
});