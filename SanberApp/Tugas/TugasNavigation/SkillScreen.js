import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'

import { AuthContext } from "./Context";

export default function SkillScreen (){
    const { logout } = React.useContext(AuthContext);

    return (
        <View style={styles.container}>
            <Text>Halaman Skill Screen</Text>
            <TouchableOpacity onPress={() => logout()}>
                <Text style={styles.button}>
                    Logout
                </Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
        width: 240,
        height: 56,
        paddingVertical: 15,
        paddingHorizontal: 90,
        borderRadius: 25,
        backgroundColor: '#3362CC',
        color: 'white',
        fontSize: 18,
        marginTop: 43
    },
});