import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

export default function ProjectScreen (){
    return (
        <View style={styles.container}>
            <Text>Halaman project screen</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
});