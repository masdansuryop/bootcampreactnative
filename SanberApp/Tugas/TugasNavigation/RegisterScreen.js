import React from 'react';
import {
    StyleSheet,
    SafeAreaView,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    StatusBar
} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'

import { AuthContext } from "./Context";

export default function RegisterScreen({ navigation }) {
    const { login } = React.useContext(AuthContext);

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.navBar}>
                <TouchableOpacity onPress={() => navigation.push("Login")}>
                    <Icon style={styles.navItem} name='arrow-back' size={25} />
                </TouchableOpacity>
            </View>
            <View style={styles.body}>
                <Text style={styles.title}>Let’s Get Started!</Text>
                <Text style={styles.subTitle}>Create an account to Sanbercode to get all features</Text>
                <View style={styles.loginForm}>
                    <TextInput style = {styles.input}
                        underlineColorAndroid = 'transparent'
                        placeholder = 'username'
                        placeholderTextColor = '#000000'
                        autoCapitalize = 'none'
                    />
                    <TextInput style = {styles.input}
                        underlineColorAndroid = 'transparent'
                        placeholder = 'password'
                        placeholderTextColor = '#000000'
                        autoCapitalize = 'none'
                        secureTextEntry={true}
                    />
                    <TextInput style = {styles.input}
                        underlineColorAndroid = 'transparent'
                        placeholder = 'password confirmation'
                        placeholderTextColor = '#000000'
                        autoCapitalize = 'none'
                        secureTextEntry={true}
                    />
                    <TouchableOpacity onPress={() => login()}>
                        <Text style={styles.button}>
                            Create
                        </Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.loginContainer}>
                    <Text>Already have an account? </Text>
                    <TouchableOpacity onPress={() => navigation.push("Login")}>
                        <Text style={styles.loginLink}>
                            Login here
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: 20,
        paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0
    },
    navBar: {
        height: 55,
        flexDirection: 'row',
        alignItems: 'center'
    },
    body: {
        alignItems: 'center',
        marginTop: 102
    },
    title: {
        fontSize: 24,
        fontWeight: '700',
    },
    subTitle: {
        fontSize: 14,
        fontWeight: '500',
    },
    loginForm: {
        marginTop: 74,
        alignItems: 'center'
    },
    input: {
        width: 374,
        height: 56,
        paddingLeft: 26,
        paddingVertical: 14,
        borderWidth: 1,
        borderRadius: 25,
        marginBottom: 19
    },
    button: {
        width: 240,
        height: 56,
        paddingVertical: 15,
        paddingHorizontal: 90,
        borderRadius: 25,
        backgroundColor: '#3362CC',
        color: 'white',
        fontSize: 18,
        marginTop: 74
    },
    loginContainer: {
        marginTop: 43,
        flexDirection: 'row'
    },
    loginLink: {
        color: '#3362CC'
    }
});