import React from 'react';
import { 
    StyleSheet,
    View,
    Image,
    Text,
    TextInput,
    TouchableOpacity
} from 'react-native'
import { AuthContext } from "./Context";

export default function LoginScreen({ navigation }) {
    const { login } = React.useContext(AuthContext);

    return (
        <View style={styles.container}>
            <Image source={require('./images/login-ilustration.png')} style={{width: 232, height: 232}} />
            <Text style={styles.title}>Welcome Back!</Text>
            <Text style={styles.subTitle}>Log in to your existant account of Sanbercode</Text>
            <View style={styles.loginForm}>
                <TextInput style = {styles.input}
                    underlineColorAndroid = 'transparent'
                    placeholder = 'username'
                    placeholderTextColor = '#000000'
                    autoCapitalize = 'none'
                />
                <TextInput style = {styles.input}
                    underlineColorAndroid = 'transparent'
                    placeholder = 'password'
                    placeholderTextColor = '#000000'
                    autoCapitalize = 'none'
                    secureTextEntry={true}
                />
                <Text>Forgot Password?</Text>
                <TouchableOpacity onPress={() => login()}>
                    <Text style={styles.button}>
                        Login
                    </Text>
                </TouchableOpacity>
            </View>
            <View style={styles.signUpContainer}>
                <Text>Don’t have an account? </Text>
                <TouchableOpacity onPress={() => navigation.push("Register")}>
                    <Text style={styles.signUpLink}>
                        Sign Up
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        fontSize: 24,
        fontWeight: '700',
    },
    subTitle: {
        fontSize: 14,
        fontWeight: '500',
    },
    loginForm: {
        marginTop: 50,
        alignItems: 'center'
    },
    input: {
        width: 374,
        height: 56,
        paddingLeft: 26,
        paddingVertical: 14,
        borderWidth: 1,
        borderRadius: 25,
        marginBottom: 19
    },
    button: {
        width: 240,
        height: 56,
        paddingVertical: 15,
        paddingHorizontal: 90,
        borderRadius: 25,
        backgroundColor: '#3362CC',
        color: 'white',
        fontSize: 18,
        marginTop: 43
    },
    signUpContainer: {
        marginTop: 43,
        flexDirection: 'row'
    },
    signUpLink: {
        color: '#3362CC'
    }
});