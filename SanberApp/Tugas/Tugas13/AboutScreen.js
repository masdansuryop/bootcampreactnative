import React from 'react';
import {
    StyleSheet,
    SafeAreaView,
    StatusBar,
    View,
    ScrollView,
    Image,
    Text,
    TouchableOpacity,
} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { Feather } from '@expo/vector-icons'

export default function AboutScreen() {
    return (
        <SafeAreaView style={styles.container}>
            <Image source={require('./images/user-photo.png')}  style={styles.userPhoto}/>
            <View style={styles.navBar}>
                <TouchableOpacity>
                    <Icon style={{color: 'white'}} name='arrow-back' size={25} />
                </TouchableOpacity>
            </View>
            <View style={styles.body}>
                <View style={styles.profileContainer}>
                    <Text style={styles.profileName}>Masdan Suryo</Text>
                    <Text style={styles.profileAddress}>Bandung, Indonesia</Text>
                    <Text style={styles.profileAge}>23 years</Text>
                </View>
                <View style={styles.socialMedia}>
                    <View style={styles.socialMediaItem}>
                        <Feather name="instagram" size={40} color="black" />
                        <Text style={styles.socialMediaTitle}>masdan.suryo</Text>
                    </View>
                    <View style={styles.socialMediaItem}>
                        <Feather name="facebook" size={40} color="black" />
                        <Text style={styles.socialMediaTitle}>masdansuryop</Text>
                    </View>
                    <View style={styles.socialMediaItem}>
                        <Feather name="twitter" size={40} color="black" />
                        <Text style={styles.socialMediaTitle}>masdan.suryo</Text>
                    </View>
                </View>
                <View style={styles.portfolio}>
                    <Text style={styles.portfolioTitle}>Portfolio</Text>
                    <ScrollView styles={styles.scrollView} horizontal={true}>
                        <Image source={require('./images/portfolio1.png')} style={styles.portfolioImage} />
                        <Image source={require('./images/portfolio2.png')} style={styles.portfolioImage} />
                        <Image source={require('./images/portfolio3.png')} style={styles.portfolioImage} />
                    </ScrollView>
                    <View style={styles.portfolioMedia}>
                        <View style={styles.mediaItem}>
                            <Feather name="github" size={40} color="black" />
                            <Text style={styles.mediaTitle}>@masdansuryop</Text>
                        </View>
                        <View style={styles.mediaItem}>
                            <Feather name="gitlab" size={40} color="black" />
                            <Text style={styles.mediaTitle}>@masdansuryop</Text>
                        </View>
                    </View>
                </View>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0
    },
    userPhoto: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: 414,
        height: 458
    },
    navBar: {
        height: 55,
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 20,
    },
    body: {
        marginHorizontal: 20,
        paddingTop: 251
    },
    profileContainer: {
        flexDirection: 'column',
        marginBottom: 19
    },
    profileName: {
        fontSize: 24,
        fontWeight: '700',
        color: 'white'
    },
    profileAddress: {
        fontSize: 12,
        fontWeight: '300',
        color: 'white'
    },
    profileAge: {
        fontSize: 12,
        fontWeight: '300',
        color: 'white'
    },
    socialMedia: {
        height: 80,
        backgroundColor: 'white',
        borderRadius: 20,
        elevation: 2,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    socialMediaItem: {
        flexDirection: 'column',
        alignItems: 'center'
    },
    socialMediaTitle: {
        fontSize: 12,
        fontWeight: '300'
    },
    portfolio: {
        flexDirection: 'column',
        paddingVertical: 22,
    },
    portfolioTitle: {
        fontSize: 14,
        fontWeight: '500',
        paddingBottom: 20
    },
    portfolioImage: {
        width: 243,
        height: 159,
        borderRadius: 25,
        marginRight: 20
    },
    portfolioMedia: {
        marginTop: 20,
        flexDirection: 'row',
        justifyContent: 'space-around',
        height: 80,
        borderRadius: 25,
        borderWidth: 1
    },
    mediaItem: {
        alignItems: 'center',
        flexDirection: 'row',
    },
    mediaTitle: {
        fontSize: 12,
        fontWeight: '300',
        marginLeft: 10
    },
});