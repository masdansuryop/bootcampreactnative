// Soal 1
console.log('Soal 1')
console.log('LOOPING PERTAMA')
var index1 = 2
while (index1 <= 20) {
    console.log(index1 + ' - I love coding')
    index1+=2
}
console.log('LOOPING KEDUA')
var index2 = 20
while (index2 >= 1) {
    console.log(index2 + ' - I will become a mobile developer')
    index2-=2
}
console.log()

// Soal 2
console.log('Soal 2')
for (var i = 1; i <= 20; i++) {
    if (i%2 !== 0) {
        if (i%3 === 0 ) {
            console.log(i + ' - I Love Coding')
        } else {
            console.log(i + ' - Santai')
        }
    } else if (i%2 === 0) {
        console.log(i + ' - Berkualitas')
    }
}
console.log()

// Soal 3
console.log('Soal 3')
var x = 1
var hash = ''
var y
do {
    y = 1
    do {
        hash += '#'
        y++
    } while (y <= 8);
    console.log(hash)
    hash = ''
    x++
} while (x <= 4);
console.log()

// Soal 4
console.log('Soal 4')
for (var i = 0; i < 7; i++) {
    hash = ''
    for (var j = 0; j <= i; j++) {
        hash += '#'
    }
    console.log(hash)
}
console.log()

// Soal 5
console.log('Soal 5')
var teks
for (let y = 1; y <= 8; y++) {
    teks = ''
    for (let z = 1; z <= 8; z++) {
        if (y%2 !== 0) {
            if (z%2 !== 0) {
                teks += ' '
            } else {
                teks += '#'
            }
        } else {
            if (z%2 !== 0) {
                teks += '#'
            } else {
                teks += ' '
            }
        }
    }    
    console.log(teks)
}